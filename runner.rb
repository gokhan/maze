#!/usr/bin/env ruby
require 'rubygems'
require 'colorize'
require './libs/shell.rb'
require './libs/maze.rb'
require './libs/exceptions.rb'

maze = Maze.new('assignment.mz')
display(maze)
$stdin.each_line do |line|
  message = ''
  line = line.strip
  #check if we have a valid command to execute
  if line =~ /((\d+)\s+)?(up|down|right|left)/
    numbes_of_steps = Integer($2 || 1)
    direction = $3
    begin
      maze.send(direction, numbes_of_steps)
    rescue HitTheRock
      message = 'Oppsss! We hit a rock.'
    rescue HitTheObstacle
      message = 'Oppsss! We hit an obstacle, destroy it first.'
    rescue OutOfRange
      message = 'Oppsss! You are trying to get out of the maze.'
    rescue ReachedExit
      message = 'Congradulations! You found the exit.'
    end
  elsif line =~ /bomb\s(\d+),?(\s+)?(\d+)/
    x, y = Integer($1)-1, Integer($3)-1
    begin
      maze.bomb(x, y)
    rescue OutOfRange
      message = 'You tried to bomb out of the maze.'
    rescue NoBomb
      message = 'You have no bomb to destroy.'
    rescue Suicide
      message = 'You have just killed yourself.'
      maze.load
    end
  elsif line =~ /restart/
    maze.load
  elsif line =~ /exit|quit/
    break
  else
    display_usage
  end
  
  display(maze, message)
end
