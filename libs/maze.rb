class Maze
  # directions
  Y = 0
  X = 1
  UP = LEFT = -1
  DOWN = RIGHT = 1
  
  MAZE_DIR = './mazes'
  
  # markes chars
  MARKERS = {
    player: '@',
    exit: 'E',
    rock: '#',
    obstacle: '-',
    road: ' ',
    bomb: 'B',
  }
  
  def initialize(file_name=nil)
    load(file_name) if file_name    
  end  

  # loads the maze and intializes
  def load(file_name='assignment.mz')
    maze = File.open([MAZE_DIR, file_name].join('/'), 'r') { |f| f.read }
    @maze = maze.split("\n").map { |row| row.split('') }    
    @bombs = 0
    find_player
  end
  
  # prints the maze and location of the user
  def print
    @maze.each do |row|
      puts colorize(row.join())
    end      
    puts "Player at #{@player[0]+1}x#{@player[1]+1} with #{@bombs} bombs"
  end
  
  # moves # of steps
  def move(direction, step, moves)
    initial = @maze.dup # lets get a copy of the maze before we start
    
    current_location = @player.clone
    # if we have an event to handle
    begin
      moves.times do |x| 
        step( direction, step )
      end
    rescue
      @maze = initial
      find_player
      raise
    end
  end
  
  # bombing a tile
  # checks first the requirements
  # destroys 3x3 tiles unless they are rock
  # kills the user if user is in the range of 5x5
  def bomb(x, y)
    raise NoBomb if @bombs == 0
    raise OutOfRange if @maze.size <= y or  @maze[0].size < x
    if (x-2..x+2).to_a.include?(@player[X])  and (y-2..y+2).to_a.include?(@player[Y]) 
       raise Suicide 
    end
    
    left, right = [ x - 1, 0 ].max, [ x + 1, @maze[0].size ].min
    top, bottom = [ y - 1, 0 ].max, [ y + 1, @maze.size ].min
    
    # lets turn obstacles to a road
    left.upto(right) do |tile_x|
      top.upto(bottom) do |tile_y|
        tile_location = [tile_y, tile_x]
        set_tile(tile_location, MARKERS[:road]) if get_tile(tile_location) == MARKERS[:obstacle]
      end
    end
    
    @bombs -= 1    
  end

  # directional moves
  def up(moves)
    move(Y, UP, moves)
  end

  def down(moves)
    move(Y, DOWN, moves)
  end  

  def right(moves)
    move(X, RIGHT, moves)
  end
  
  def left(moves)
    move(X, LEFT, moves)
  end
  
  private
  
  # updates the tile in case of player moving over/bombing
  def set_tile(location, char)
    @maze[location[Y]][location[X]] = char 
  end
  
  # gets the tile of the location
  def get_tile(location)
    @maze[location[Y]][location[X]]
  end
  
  # move player one step in the direction user commands
  def step(direction, step)
    previous_location = @player.dup
    @player[direction] += step
    
    # check if we hit something
    @bombs += 1 if get_tile(@player) == MARKERS[:bomb]
    raise HitTheRock if get_tile(@player) == MARKERS[:rock]
    raise HitTheObstacle if get_tile(@player) == MARKERS[:obstacle]
    raise OutOfRange if @player[X] >= @maze[0].size or @player[Y] >= @maze.size
    if get_tile(@player) == MARKERS[:exit]
      set_tile( previous_location, MARKERS[:road] )
      set_tile( @player, MARKERS[:player] )
      raise ReachedExit 
    end

    # update the maze unless we are on the wrong tile
    set_tile( previous_location, MARKERS[:road] )
    set_tile( @player, MARKERS[:player] )
  end    
  
  # Locate the player location in our maze
  def find_player
    player_found = false
    @maze.each_with_index do |row, row_index|
      row.each_with_index do |col, col_index|
        if col == MARKERS[:player] 
          raise 'Too many players. This is 1 person game' if player_found
          player_found = true
          @player = [row_index, col_index]
        end
      end
    end
    raise 'Where is the player?' unless player_found
  end
  
  # Returns the maze colorized
  def colorize(text)
    text.gsub('B', 'B'.colorize(:red))
        .gsub('E', 'E'.colorize(:blue))
        .gsub('@', '@'.colorize(color: :white, background: :black))
        .gsub('-', '-'.colorize(:yellow))
  end
  
end