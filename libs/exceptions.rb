class HitTheRock < StandardError; end
class HitTheObstacle < StandardError; end
class ReachedExit < StandardError; end
class OutOfRange < StandardError; end
class NoBomb < StandardError; end
class Suicide < StandardError; end