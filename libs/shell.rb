def display_usage
  puts 'Command not recognized. Command list;'
  puts [' up|down|right|left', ' 3 up|down|right|left', ' bomb 3 3', ' quit']
end

def clear
  system('clear') or system('cls')
end

def display(maze, message = '')
  clear
  maze.print
  puts message if message != ''
  print '>'
end