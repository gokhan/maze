# Technical challenge: Bomb the Maze!

Player must obtain explosives and bomb their way out of the maze.

## Map

```
###################E####################
################### ####################
################### ####################
#########B    ---    ###################
#########   ###-########################
#########---###-########################
#########---###                -########
#########---###################-########
#########---###################-########
#########---                    ########
####################@###################
#################### ###################
#################### ###################
#################### ###################
####################B              #####
####################---#########   #####
####################---#########   #####
####################---#########   #####
###########BB          #########   #####
########################################
```


## Key

 Character | Meaning  
---|----------------------
E | exit; middle of line 1
# | indestructible structure
- | destructible structure
B | bomb, move onto it to pick it up
@ | your position; you start at line 11

## Rules

1. The program should always display an up-to-date view of the world. If you destroy something with a bomb, it should no longer be there. If you move, the `@` character should be displayed in your position.
2. You cannot pass through `#` and `-`. Anything beyond the visible map counts as `#`. A space is a perfectly passable square.
3. You can destroy `-` with a bomb.
4. A bomb destroys all destructible structures within a 3x3 square (where the middle of the square is the drop position). The bomb kills a player if they are within a 5x5 square.
5. A player can pick up a bomb by moving onto it.
6. The movement and other actions in the game are done via commands that need to be typed and sent with Enter/Return.
7. In order to move, player must type `up`, `left`, `right` or `down`.
8. It should be possible to type `5 up` to move 5 squares up. Any bomb found across the path should be picked automatically. If the destination position is not available (because it's `#` or `-`), the command should be ignored.
9. In order to bomb a position, player must type `bomb _col_ _row_` where  `_col_` and `_row_` are numbers representing column and row. `bomb 1 1` will bomb top left corner. `bomb 40 20` will bomb bottom right corner.
10. Player can type `bombs` to check how many bombs they have. Player starts with 0 bombs.
11. Player can type `restart` to start over.
12. Player must reach `E` to win the game.

## Example solution

1. Pick up the bomb in row 15.
2. Move up one square so that the shock wave doesn't kill you.
3. Bomb row 17.
4. Pick up two bombs in row 19.
5. Bomb row 8.
6. Bomb row 5.
7. Reach the exit


Note that this game is visual and it's expected that it reads input from keyboard and refreshes the world on screen. It's up to you to display the UI but the game is really simple so all you really need is command prompt and the current world map.

Here's what user can type in order to reach the exit. 

```
4 down
up
bomb 22, 17
5 down
9 left
bomb 32 8
bomb 16 5
9 right
9 up
11 right
3 up
16 left
3 up
4 right
3 up
```